import mangoes
import pickle
import json
from sklearn.cluster import KMeans
import numpy as np
from scipy.spatial.distance import cosine
from tqdm import tqdm
from scipy.stats import spearmanr
from preprocessing.preprocess_english import get_data

RANDOM_STATE = 10


def get_max_sim_score(centroids_1, centroids_2):
    max_sim = -1
    for i in range(centroids_1.shape[0]):
        for j in range(centroids_2.shape[0]):
            current_sim = 1.0 - cosine(centroids_1[i], centroids_2[j])
            if current_sim > max_sim:
                max_sim = current_sim
    return max_sim


def get_all_embeddings(model, sentences, word):
    embeddings = {}  # dict of layer_index -> ndarray of size (num_sentences, dim)
    for sentence in sentences:
        word_index = sentence.index(word)
        tok_out = model.tokenizer.tokenize(' '.join(sentence))
        if len(tok_out) + 2 > 512:
            continue
        hidden_states = model.generate_outputs(sentence, pre_tokenized=True, output_hidden_states=True,
                                               word_embeddings=True)["hidden_states"]
        for layer_i in range(len(hidden_states)):
            if layer_i not in embeddings:
                embeddings[layer_i] = []
            embeddings[layer_i].append(hidden_states[layer_i][0, word_index].cpu().detach().numpy())
    for layer_i in embeddings.keys():
        embeddings[layer_i] = np.asarray(embeddings[layer_i])
    return embeddings


def compute_all_centroids(embeddings, n_clusters):
    # embeddings: dict of layer_index -> list of numpy arrays
    # returns dict of n_cluster -> layer -> ndarray of size (n_cluster, embedding_size)
    layer_indices = list(embeddings.keys())
    centroids = {}
    for k in n_clusters:
        for layer_i in layer_indices:
            k_means = KMeans(n_clusters=k, random_state=RANDOM_STATE, init='k-means++',
                             n_init=2).fit(embeddings[layer_i])
            if layer_i not in centroids:
                centroids[layer_i] = k_means.cluster_centers_
            else:
                centroids[layer_i] = np.concatenate((centroids[layer_i], k_means.cluster_centers_))
    return centroids


if __name__ == "__main__":
    with open("./data/wikipedia/hyperlex_sentences.pkl", 'rb') as f:
        sentences = pickle.load(f)
    dataset, all_words, _ = get_data()
    n_clusters = list(range(1, 11))
    model = mangoes.modeling.TransformerForFeatureExtraction("bert-base-uncased", "bert-base-uncased", device=None)
    all_scores = {}
    all_similarities = {}
    for word1, word2 in tqdm(dataset.keys()):
        embeddings = get_all_embeddings(model, sentences[word1], word1)
        word1_centroids = compute_all_centroids(embeddings, n_clusters)
        embeddings = get_all_embeddings(model, sentences[word2], word2)
        word2_centroids = compute_all_centroids(embeddings, n_clusters)
        for layer_i in word1_centroids.keys():
            if layer_i not in all_scores:
                all_scores[layer_i] = {}
                all_similarities[layer_i] = {}
            all_similarities[layer_i][(word1, word2)] = get_max_sim_score(word1_centroids[layer_i],
                                                                          word2_centroids[layer_i])

    sorted_keys = sorted(list(dataset.keys()))
    for layer_i in all_similarities.keys():
        all_scores[layer_i] = spearmanr([dataset[x] for x in sorted_keys],
                                        [all_similarities[layer_i][x] for x in sorted_keys])

    with open("./multi_prototype_allclusters_scores.json", 'w') as f:
        json.dump(all_scores, f)

    for layer_i in all_similarities.keys():
        all_similarities[layer_i] = {f"{k[0]}_{k[1]}": v for k, v in
                                     all_similarities[layer_i].items()}

    with open("./multi_prototype_allclusters_sims.json", 'w') as f:
        json.dump(all_similarities, f)
