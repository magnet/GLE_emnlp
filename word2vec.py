from scipy.spatial.distance import cosine
from gensim.models import KeyedVectors
import numpy as np
from scipy.stats import spearmanr
from preprocessing.preprocess_english import get_data


dataset, all_words, pos = get_data()
dataset = {k: v for k, v in dataset.items() if pos[k] == 'N'}

model = KeyedVectors.load_word2vec_format("word_embeddings/GoogleNews-vectors-negative300.bin", binary=True)
w2v_dataset_scores = {}  # dict of dict of wordpairs -> entailment score
for word1, word2 in dataset.keys():
    prototype_words = word1.replace('_', ' ').lower().split(' ')
    prototype_emb = np.mean(np.array([model.get_vector(w) for w in prototype_words if model.has_index_for(w)]), axis=0)
    if all(model.has_index_for(w) for w in word2.split(' ')):
        w2v_dataset_scores[(word1, word2)] = 1 - (cosine(np.mean(np.array([model.get_vector(w) for w in word2.split(' ')]), axis=0),
                                                             prototype_emb))
    else:
        print(f"{list(w for w in word2.split(' ') if not model.has_index_for(w))} ({word1}) not in model vocab")

sorted_keys = sorted(list(dataset.keys()))
score = spearmanr([dataset[x] for x in sorted_keys], [w2v_dataset_scores[x] for x in sorted_keys])
print(score)
