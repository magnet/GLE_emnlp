from preprocessing.preprocess_english import get_data, get_relations, get_lexical_splits, get_random_splits
from scipy.stats import spearmanr
import numpy as np
import mangoes
from sklearn import linear_model
from tqdm import tqdm
import pickle
import os


def get_hidden_states(datasets):
    if os.path.exists('hyperlex_hidden_states_bert_base_uncased.pickle'):
        with open('hyperlex_hidden_states_bert_base_uncased.pickle', 'rb') as handle:
            hidden_states = pickle.load(handle)
    else:
        hidden_states = {}
        for dataset in datasets:
            for word1, word2 in tqdm(list(dataset.keys())):
                if word1 not in hidden_states:
                    hidden_states[word1] = [x.cpu().numpy() for x in model.generate_outputs([word1], pre_tokenized=True,
                                                                                            output_hidden_states=True,
                                                                                            word_embeddings=True)[
                        "hidden_states"]]
                if word2 not in hidden_states:
                    hidden_states[word2] = [x.cpu().numpy() for x in model.generate_outputs([word2], pre_tokenized=True,
                                                                                            output_hidden_states=True,
                                                                                            word_embeddings=True)[
                        "hidden_states"]]
        with open('hyperlex_hidden_states_bert_base_uncased.pickle', 'wb') as handle:
            pickle.dump(hidden_states, handle)
    return hidden_states


def preprocess_data(dataset, hidden_states, layer_i, combo="concatenate"):
    X = []
    Y = []
    for word1, word2 in dataset.keys():
        hidden_state1 = hidden_states[word1][layer_i]
        hidden_state2 = hidden_states[word2][layer_i]
        if combo == "concatenate":
            X.append(np.concatenate([hidden_state1[0, 0],
                                     hidden_state2[0, 0]], axis=0))
        elif combo == "sum":
            X.append(hidden_state1[0, 0] + hidden_state2[0, 0])
        elif combo == "diff":
            X.append(hidden_state2[0, 0] - hidden_state1[0, 0])
        Y.append(dataset[(word1, word2)])
    return X, Y


model = mangoes.modeling.TransformerForFeatureExtraction("bert-base-uncased", "bert-base-uncased", device=None)
train_data, valid_data, test_data = get_random_splits()

hidden_states = get_hidden_states([train_data, valid_data, test_data])

combos = ["concatenate", "sum", "diff"]
layer_is = range(13)


"""for combo in combos:
    best_spearman = -10000
    best_hyperparams = (None, None)
    for layer_i in layer_is:
        train_x, train_y = preprocess_data(train_data, hidden_states, layer_i, combo)
        valid_x, valid_y = preprocess_data(valid_data, hidden_states, layer_i, combo)
        test_x, test_y = preprocess_data(test_data, hidden_states, layer_i, combo)

        reg = linear_model.LinearRegression()
        reg.fit(train_x, train_y)
        valid_pred = reg.predict(valid_x)
        score = spearmanr(valid_y, valid_pred)

        if score[0] > best_spearman:
            best_hyperparams = (combo, layer_i)
            best_spearman = score[0]
            test_pred = reg.predict(test_x)
            score_test = spearmanr(test_y, test_pred)
            print(score_test[0])
            print(best_hyperparams)"""

alphas = [0.0, 0.5, 1.0, 2.0, 5.0, 10.0]
for combo in combos:
    best_spearman = -10000
    best_hyperparams = (None, None)
    for layer_i in layer_is:
        for alpha in alphas:
            train_x, train_y = preprocess_data(train_data, hidden_states, layer_i, combo)
            valid_x, valid_y = preprocess_data(valid_data, hidden_states, layer_i, combo)
            test_x, test_y = preprocess_data(test_data, hidden_states, layer_i, combo)

            reg = linear_model.Ridge(alpha=alpha)
            reg.fit(train_x, train_y)
            valid_pred = reg.predict(valid_x)
            score = spearmanr(valid_y, valid_pred)

            if score[0] > best_spearman:
                best_hyperparams = (combo, layer_i, alpha)
                best_spearman = score[0]
                test_pred = reg.predict(test_x)
                score_test = spearmanr(test_y, test_pred)
                print(score_test[0])
                print(best_hyperparams)

