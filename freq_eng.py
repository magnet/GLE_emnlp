import pickle as pkl
from scipy.stats import spearmanr
from scipy.spatial.distance import cosine
from gensim.models import KeyedVectors
import numpy as np
from preprocessing.preprocess_english import get_data


def freq_sim(freq_1, freq_2, alpha, word_sim=None):
    if word_sim is not None and word_sim < 0.1:
        return 0.0
    return 1.0 - ((float(freq_1) + alpha) / float(freq_2))


dataset, all_words, pos = get_data()
ALPHA = 0.0

model = KeyedVectors.load_word2vec_format("word_embeddings/GoogleNews-vectors-negative300.bin", binary=True)

with open("./data/wikipedia/wiki_word_count.pkl", 'rb') as f:
    words_count = pkl.load(f)

freqs = {}
for word1, word2 in dataset.keys():
    if all(model.has_index_for(w) for w in word2.split(' ')) and all(model.has_index_for(w) for w in word1.split(' ')):
        word_sim = 1 - (cosine(np.mean(np.array([model.get_vector(w) for w in word2.split(' ')]), axis=0),
                               np.mean(np.array([model.get_vector(w) for w in word1.split(' ')]), axis=0)))
    else:
        print(f"{list(w for w in word1.split(' ') if not model.has_index_for(w))} ({word1}) not in model vocab")

        print(f"{list(w for w in word2.split(' ') if not model.has_index_for(w))} ({word2}) not in model vocab")
        word_sim = 0.0
    if len(word2.split(" ")) == 1 and len(word1.split(" ")) == 1:
        if word1 in words_count and word2 in words_count:
            freqs[(word1, word2)] = freq_sim(words_count[word1], words_count[word2], ALPHA, word_sim)
        else:
            freqs[(word1, word2)] = 0

sorted_keys = sorted(list(dataset.keys()))
score = spearmanr([dataset[x] for x in sorted_keys], [freqs[x] for x in sorted_keys])
print(score)

noun_dataset = {k: v for k, v in dataset.items() if pos[k] == 'N'}
sorted_keys = sorted(list(noun_dataset.keys()))

score = spearmanr([dataset[x] for x in sorted_keys], [freqs[x] for x in sorted_keys])
print(f"nouns: {score[0]}")