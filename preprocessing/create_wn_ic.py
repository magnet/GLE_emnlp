import mangoes
import pickle as pkl
from nltk.corpus import wordnet as wn

PATH_TO_WIKIPEDIA_CORPUS = ""


class ICCorpus:
    def __init__(self):
        self.corpus = mangoes.Corpus(PATH_TO_WIKIPEDIA_CORPUS,  lower=True, lazy=True)

    def words(self):
        for sent in self.corpus:
            for word in sent:
                yield word


wiki_corp = ICCorpus()
new_ic = wn.ic(wiki_corp, weight_senses_equally=False, smoothing=0.0)

with open("wiki_ic.pkl", 'wb') as f:
    pkl.dump(new_ic, f)



