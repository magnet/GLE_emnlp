
def get_data(data_file="./data/hyperlex-data/hyperlex-all.txt"):
    dataset = {}
    pos = {}
    all_words = set()
    with open(data_file, 'r') as f:
        lines = f.readlines()
    for l in lines:
        tokens = l.split()
        if not tokens[0] == "WORD1":
            all_words.add(tokens[0])
            all_words.add(tokens[1])
            dataset[(tokens[0], tokens[1])] = float(tokens[4])
            pos[(tokens[0], tokens[1])] = tokens[2]
    return dataset, all_words, pos


def get_relations():
    relations = {}
    with open("./data/hyperlex-data/hyperlex-all.txt", 'r') as f:
        lines = f.readlines()
    for l in lines:
        tokens = l.split()
        if not tokens[0] == "WORD1":
            relations[(tokens[0], tokens[1])] = tokens[3]
    return relations


def get_lexical_splits():
    train_data = get_data(data_file="./data/hyperlex-data/splits/lexical/hyperlex_training_all_lexical.txt")[0]
    valid_data = get_data(data_file="./data/hyperlex-data/splits/lexical/hyperlex_dev_all_lexical.txt")[0]
    test_data = get_data(data_file="./data/hyperlex-data/splits/lexical/hyperlex_test_all_lexical.txt")[0]
    return train_data, valid_data, test_data


def get_random_splits():
    train_data = get_data(data_file="./data/hyperlex-data/splits/random/hyperlex_training_all_random.txt")[0]
    valid_data = get_data(data_file="./data/hyperlex-data/splits/random/hyperlex_dev_all_random.txt")[0]
    test_data = get_data(data_file="./data/hyperlex-data/splits/random/hyperlex_test_all_random.txt")[0]
    return train_data, valid_data, test_data


def get_semeval_testsplit():
    dataset, all_words, pos = get_data()

    with open("data/hyperlex-data/SemEval_testset.txt", 'r') as f:
        lines = f.readlines()

    all_pairs = []
    for pair in lines:
        all_pairs.append((pair.split()[0], pair.split()[1]))

    all_pairs = set(all_pairs)
    dataset = {k: v for k, v in dataset.items() if k in all_pairs}
    return dataset
