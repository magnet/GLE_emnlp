from nltk.corpus import wordnet as wn
import nltk
from preprocessing.preprocess_english import get_data
import pickle


def find_lcs(synset1, synset2):
    need_root = synset1._needs_root() or synset2._needs_root()
    subsumers = synset1.lowest_common_hypernyms(synset2, simulate_root=need_root, use_min_depth=True)
    if len(subsumers) == 0:
        return None
    return synset1 if synset1 in subsumers else subsumers[0]


def save_wordnet_lcs():
    nltk.download('wordnet_ic')
    with open("./data/wikipedia/wiki_ic.pkl", 'rb') as f:
        wiki_ic = pickle.load(f)
    dataset, all_words, pos = get_data()

    lcs = {}  # dict of dataset name -> dict of word -> lcs
    for word1, word2 in dataset.keys():
        poss_synsets_1 = [x for x in wn.synsets(word1.replace(" ", "_")) if x._pos == pos[(word1, word2)].lower()]
        poss_synsets_2 = [x for x in wn.synsets(word2.replace(" ", "_")) if x._pos == pos[(word1, word2)].lower()]
        if len(poss_synsets_1) > 0 and len(poss_synsets_2) > 0:
            max_sim = -1000
            synsets = None
            for syn1 in poss_synsets_1:
                for syn2 in poss_synsets_2:
                    if syn1._pos == syn2._pos:
                        curr_sim = syn1.lin_similarity(syn2, wiki_ic)
                        if curr_sim > max_sim:
                            max_sim = curr_sim
                            synsets = (syn1, syn2)
            lcs[(word1, word2)] = find_lcs(synsets[0], synsets[1]).name()
    with open(f"data/hyperlex_wn_lcs.pkl", 'wb') as f:
        pickle.dump(lcs, f)


if __name__ == "__main__":
    save_wordnet_lcs()
