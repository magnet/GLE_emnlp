# GradedLexicalEntailmentEMNLP

This repository is the code and data needed to run the methods and experiments from the EMNLP paper "WordNet Is All You Need: A Surprisingly Effective Unsupervised Method for Graded Lexical Entailment" by Joseph Renner, Pascal Denis and Rémi Gilleron.


