from nltk.corpus import wordnet as wn
from nltk.corpus.reader.wordnet import information_content
import nltk
import pickle
from scipy.stats import spearmanr
from preprocessing.preprocess_english import get_data, get_relations
from statistics import mean


def is_hyp1(syn1, syn2):
    return syn2 in syn1.common_hypernyms(syn2) and syn1.shortest_path_distance(syn2) == 1


def is_hyp2(syn1, syn2):
    return syn2 in syn1.common_hypernyms(syn2) and syn1.shortest_path_distance(syn2) == 2


def is_hyp3(syn1, syn2):
    return syn2 in syn1.common_hypernyms(syn2) and syn1.shortest_path_distance(syn2) == 3


def is_hyp4plus(syn1, syn2):
    return syn2 in syn1.common_hypernyms(syn2) and syn1.shortest_path_distance(syn2) > 3


def is_rhyp1(syn1, syn2):
    return syn1 in syn1.common_hypernyms(syn2) and syn1.shortest_path_distance(syn2) == 1


def is_rhyp2(syn1, syn2):
    return syn1 in syn1.common_hypernyms(syn2) and syn1.shortest_path_distance(syn2) == 2


def is_rhyp3(syn1, syn2):
    return syn1 in syn1.common_hypernyms(syn2) and syn1.shortest_path_distance(syn2) == 3


def is_rhyp4plus(syn1, syn2):
    return syn1 in syn1.common_hypernyms(syn2) and syn1.shortest_path_distance(syn2) > 3


def is_ant(syn1, syn2):
    return syn1 in [ant.synset() for lemma in syn2.lemmas() for ant in lemma.antonyms()] or \
           syn2 in [ant.synset() for lemma in syn1.lemmas() for ant in lemma.antonyms()]


def is_syn(syn1, syn2):
    return syn1 == syn2


def is_cohyp(syn1, syn2, lcs):
    return lcs is not None and syn1.shortest_path_distance(lcs) == 1 and syn2.shortest_path_distance(lcs) == 1


def is_mero(syn1, syn2):
    return syn1 in syn2.part_meronyms() + syn2.substance_meronyms()


def no_rel(lcs):
    return lcs is None or lcs.name == "*ROOT*"


true_relations = get_relations()
dataset, all_words, pos = get_data()

nltk.download('wordnet_ic')
with open("./data/wikipedia/wiki_ic.pkl", 'rb') as f:
    wiki_ic = pickle.load(f)

wsd = True
wn_dataset_scores = {}
wn_dataset_sims = {}
wn_dataset_swics = {}
ics = {}
for word1, word2 in dataset.keys():
    poss_synsets_1 = [x for x in wn.synsets(word1.replace(" ", "_")) if x._pos == pos[(word1, word2)].lower()]
    poss_synsets_2 = [x for x in wn.synsets(word2.replace(" ", "_")) if x._pos == pos[(word1, word2)].lower()]
    if len(poss_synsets_1) > 0 and len(poss_synsets_1) > 0:
        if wsd:
            max_score = (-1000, None)
        else:
            max_score = [0.0, 0.0, 0.0]
        total_synset_pairs = 0
        for syn1 in poss_synsets_1:
            for syn2 in poss_synsets_2:
                if syn1._pos == syn2._pos:
                    syn1_ic = information_content(syn1, wiki_ic)
                    syn2_ic = information_content(syn2, wiki_ic)
                    sim = syn1.lin_similarity(syn2, wiki_ic)
                    curr_score = 1 - (syn2_ic / syn1_ic) + sim
                    subsumers = syn1.common_hypernyms(syn2)
                    if len(subsumers) == 0:
                        lcs = None
                    else:
                        lcs = sorted(subsumers, reverse=True, key=lambda x: information_content(x, wiki_ic))[0]
                    if wsd:
                        if sim > max_score[0]:
                            max_score = (sim, curr_score, 1 - (syn2_ic / syn1_ic), syn1_ic, syn2_ic, syn1, syn2, lcs)
                    else:
                        max_score[0] += sim
                        max_score[1] += curr_score
                        max_score[2] += 1 - (syn2_ic / syn1_ic)
                        total_synset_pairs += 1
        wn_dataset_scores[(word1, word2)] = max_score[1] / (1.0 if wsd else float(total_synset_pairs))
        wn_dataset_sims[(word1, word2)] = max_score[0] / (1.0 if wsd else float(total_synset_pairs))
        wn_dataset_swics[(word1, word2)] = max_score[2] / (1.0 if wsd else float(total_synset_pairs))


sorted_keys = sorted(list(wn_dataset_scores.keys()))

score = spearmanr([dataset[x] for x in sorted_keys], [wn_dataset_swics[x] for x in sorted_keys])
print(f"swic: {score[0]}")

score = spearmanr([dataset[x] for x in sorted_keys], [wn_dataset_sims[x] for x in sorted_keys])
print(f"sim: {score[0]}")

score = spearmanr([dataset[x] for x in sorted_keys], [wn_dataset_scores[x] for x in sorted_keys])
print(f"full: {score[0]}")

noun_dataset = {k: v for k, v in dataset.items() if pos[k] == 'N'}
sorted_keys = sorted(list(noun_dataset.keys()))

score = spearmanr([dataset[x] for x in sorted_keys], [wn_dataset_swics[x] for x in sorted_keys])
print(f"swic: {score[0]}")

score = spearmanr([dataset[x] for x in sorted_keys], [wn_dataset_sims[x] for x in sorted_keys])
print(f"sim: {score[0]}")

score = spearmanr([dataset[x] for x in sorted_keys], [wn_dataset_scores[x] for x in sorted_keys])
print(f"full nouns: {score[0]}")


