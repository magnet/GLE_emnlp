from mangoes.modeling import TransformerModel
from transformers import AutoTokenizer, AutoModelForMaskedLM
import pickle
from tqdm import tqdm
import numpy as np
from torch.nn.functional import softmax
from scipy.stats import spearmanr
from minicons import scorer
from preprocessing.preprocess_english import get_data


def a_or_an(word, capital):
    if word[0] in ["a", "e", "i", "o", "u"]:
        return "An" if capital else "an"
    else:
        return "A" if capital else "a"


dataset, all_words, pos = get_data()
transformer = scorer.MaskedLMScorer("bert-base-uncased", "cpu")
pred_scores = {}
for word1, word2 in tqdm(dataset.keys()):
    premise = f"{a_or_an(word1, capital=True)} {word1} is "
    conclusion = f"{a_or_an(word2, capital=False)} {word2}."
    priming_score = transformer.compute_stats(transformer.prime_text(premise, conclusion))[0]
    pred_scores[(word1, word2)] = np.sum(priming_score)

sorted_keys = sorted(list(dataset.keys()))

score = spearmanr([dataset[x] for x in sorted_keys], [pred_scores[x] for x in sorted_keys])
print(f"full: {score[0]}")

noun_dataset = {k: v for k, v in dataset.items() if pos[k] == 'N'}
sorted_keys = sorted(list(noun_dataset.keys()))
score = spearmanr([dataset[x] for x in sorted_keys], [pred_scores[x] for x in sorted_keys])
print(f"nouns: {score[0]}")
